import java.util.*;
public class matriks extends theMatrix{
	static Scanner input=new Scanner(System.in);
	
	 void MainMenu(){
		 matriks panggil = new matriks();
		while(true){
			
		
			int select;
			System.out.println("\t\t   MATRIX");
			System.out.println("===========================================");			
			System.out.println("1. Addition");
			System.out.println("2. Subtraction");
			System.out.println("3. Multiplication");
			System.out.println("0. Exit");
			System.out.println("===========================================");	
			System.out.println("Please select a number");
			do{
				System.out.print(">> ");
				try{
					select=input.nextInt();
					if(select!= 1 && select!= 2 && select!= 3  && select!= 0 ){
						System.out.println("Please select only the '1', '2', '3', or '0' number");
						continue;
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Please select only the '1', '2', '3', or '0' number");
					input.nextLine();
					continue;
				}
			}while(true);
			
			if(select== 0){
				break;
			}
			
			switch(select){
				case 1:
					panggil.addition();
					break;
				case 2:
					panggil.substraction();
					break;
				case 3:
					panggil.multiplication();
					break;
				
				
			}
			
		}
		
		
	}
	
	 void addition(){
		 
		int baris,kolom;
		do{
			System.out.println("\t\t  Addition");
			System.out.println("===========================================");
				
			do{	
				try{
					
					System.out.println("Input the number of baris of the matrices ");
					System.out.print(">> ");
					baris=input.nextInt();
					
						if (baris <1){
							throw new InputMismatchException();
						}
					System.out.println("Input the number of kolom of the matrices ");
					System.out.print(">> ");
					kolom=input.nextInt();
					
						if (kolom <1){
							throw new InputMismatchException();
						}
					break;
				}catch(InputMismatchException e){
					System.out.println("Please input a natural number ( > 0 )");
					input.nextLine();
					continue;
				}
			}while(true);
			
			double[][]matriks1= new double[baris][kolom];
			double[][]matriks2= new double[baris][kolom];
			double[][]matrikshasil=new double[baris][kolom];
			System.out.println("\t\t MATRIX 1");
			do{
				try{
					for(int isibaris=0; isibaris<baris;isibaris++){
						for(int isikolom=0; isikolom<kolom;isikolom++){
							System.out.println("Input the value of row "+(isibaris+1)+" and column "+(isikolom+1));
							System.out.print(">> ");
							matriks1[isibaris][isikolom]=input.nextDouble();
							
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Please input a real number");
					input.nextLine();
					continue;
				}	
			}while(true);
			
			System.out.println();
			for(int isibaris=0; isibaris<baris;isibaris++){
				for(int isikolom=0; isikolom<kolom;isikolom++){
					
					System.out.printf("%.2f  ",matriks1[isibaris][isikolom]);
				}System.out.println();
			}
			
			
			System.out.println();
			
			System.out.println("\t\t MATRIX 2");
			do{
				try{
					for(int isibaris=0; isibaris<baris;isibaris++){
						for(int isikolom=0; isikolom<kolom;isikolom++){
							System.out.println("Please input the value of row "+(isibaris+1)+" and column "+(isikolom+1));
							System.out.print(">> ");
							matriks2[isibaris][isikolom]=input.nextDouble();
							
							matrikshasil[isibaris][isikolom]=matriks1[isibaris][isikolom] + matriks2[isibaris][isikolom];
							
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Please input a real number");
					input.nextLine();
					continue;
				}	
			}while(true);
			
			System.out.println();
			for(int isibaris=0; isibaris<baris;isibaris++){
				for(int isikolom=0; isikolom<kolom;isikolom++){
					
					System.out.printf("%.2f  ",matriks2[isibaris][isikolom]);
				}System.out.println();
			}
			
			System.out.println("\n");
			System.out.println("The result of the addition of both matrices is ");
			System.out.println();
			for(int isibaris=0; isibaris<baris;isibaris++){
				for(int isikolom=0; isikolom<kolom;isikolom++){
					this.matriks1=matriks1[isibaris][isikolom];
					this.matriks2=matriks2[isibaris][isikolom];
					
					System.out.printf("%.2f",add());
				}System.out.println();
			}
			
			
			
			if(baris != kolom){
				System.out.println("The result of the matrix doesn't have a determinant");
			}	
			else{
				
				if(baris==2 && kolom==2){
					if(((matrikshasil[0][0]*matrikshasil[1][1])-(matrikshasil[0][1]*matrikshasil[1][0]))!=0){
						System.out.println("The determinant of the matrix is "+((matrikshasil[0][0]*matrikshasil[1][1])-(matrikshasil[0][1]*matrikshasil[1][0])));
					}
					else{
						System.out.println("It's a mirrored matrix");
					}
				}
				else if(baris==3 && kolom==3){
					if(((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
							-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
							+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1]))))!=0){
						System.out.println("The determinant of the matrix is "+((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
								-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
								+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1])))));
					}
					else{
						System.out.println("It's a mirrored matrix");
					}
				}
			}
			
			
			
			
			System.out.println();
			String pilihan;
			input.nextLine();
			do{
				System.out.println("Try again ? (y/n)");
				pilihan=input.nextLine();
				if (pilihan.contentEquals("n")){
					 break;
				 }
				 else  if (pilihan.contentEquals("y")){
					 break;
				 }
				 else{
					 System.out.println("Please input only the letter 'y' or 'n'");
					 continue;
				 }
			}while(true);
			 if (pilihan.contentEquals("n")){
				 break;
			 }
			 else  if (pilihan.contentEquals("y")){
				 continue;
			 }
			
		}while(true);
		
		
	}
	
	 void substraction(){
		int baris,kolom;
		do{
			System.out.println("\t\t  Subtraction");
			System.out.println("===========================================");
		
			do{	
				try{
					
					System.out.println("Input the number of baris of the matrices ");
					System.out.print(">> ");
					baris=input.nextInt();
					
						if (baris <1){
							throw new InputMismatchException();
						}
					System.out.println("Input the number of kolom of the matrices ");
					System.out.print(">> ");
					kolom=input.nextInt();
					
						if (kolom <1){
							throw new InputMismatchException();
						}
					break;
				}catch(InputMismatchException e){
					System.out.println("Please input a natural number ( > 0 )");
					input.nextLine();
					continue;
				}
			}while(true);
			
			double[][]matriks1= new double[baris][kolom];
			double[][]matriks2= new double[baris][kolom];
			double[][]matrikshasil=new double[baris][kolom];
			System.out.println("\t\t MATRIX 1");
			do{
				try{
					for(int isibaris=0; isibaris<baris;isibaris++){
						for(int isikolom=0; isikolom<kolom;isikolom++){
							System.out.println("Input the value of row "+(isibaris+1)+" and column "+(isikolom+1));
							System.out.print(">> ");
							matriks1[isibaris][isikolom]=input.nextDouble();
							
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Please input a real number");
					input.nextLine();
					continue;
				}	
			}while(true);
			
			System.out.println();
			for(int isibaris=0; isibaris<baris;isibaris++){
				for(int isikolom=0; isikolom<kolom;isikolom++){
					
					System.out.printf("%.2f  ",matriks1[isibaris][isikolom]);
				}System.out.println();
			}
			
			
			System.out.println();
			
			System.out.println("\t\t MATRIX 2");
			do{
				try{
					for(int isibaris=0; isibaris<baris;isibaris++){
						for(int isikolom=0; isikolom<kolom;isikolom++){
							System.out.println("Please input the value of row "+(isibaris+1)+" and column "+(isikolom+1));
							System.out.print(">> ");
							matriks2[isibaris][isikolom]=input.nextDouble();
							
							matrikshasil[isibaris][isikolom]=matriks1[isibaris][isikolom] + matriks2[isibaris][isikolom];
							
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Please input a real number");
					input.nextLine();
					continue;
				}	
			}while(true);
			
			System.out.println();
			for(int isibaris=0; isibaris<baris;isibaris++){
				for(int isikolom=0; isikolom<kolom;isikolom++){
					
					System.out.printf("%.2f  ",matriks2[isibaris][isikolom]);
				}System.out.println();
			}
			
			System.out.println("\n");
			System.out.println("The result of the substraction of both matrices is ");
			System.out.println();
			for(int isibaris=0; isibaris<baris;isibaris++){
				for(int isikolom=0; isikolom<kolom;isikolom++){
					this.matriks1=matriks1[isibaris][isikolom];
					this.matriks2=matriks2[isibaris][isikolom];
					
					System.out.printf("%.2f ",substract());
				}System.out.println();
			}
			
			
			
			if(baris != kolom){
				System.out.println("The result of the matrix doesn't have a determinant");
			}	
			else{
				
				if(baris==2 && kolom==2){
					if(((matrikshasil[0][0]*matrikshasil[1][1])-(matrikshasil[0][1]*matrikshasil[1][0]))!=0){
						System.out.println("The determinant of the matrix is "+((matrikshasil[0][0]*matrikshasil[1][1])-(matrikshasil[0][1]*matrikshasil[1][0])));
					}
					else{
						System.out.println("It's a mirrored matrix");
					}
				}
				else if(baris==3 && kolom==3){
					if(((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
							-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
							+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1]))))!=0){
						System.out.println("The determinant of the matrix is "+((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
								-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
								+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1])))));
					}
					else{
						System.out.println("It's a mirrored matrix");
					}
				}
			}
			
			
			
			
			System.out.println();
			String pilihan;
			input.nextLine();
			do{
				System.out.println("Try again ? (y/n)");
				pilihan=input.nextLine();
				if (pilihan.contentEquals("n")){
					 break;
				 }
				 else  if (pilihan.contentEquals("y")){
					 break;
				 }
				 else{
					 System.out.println("Please input only the letter 'y' or 'n'");
					 continue;
				 }
			}while(true);
			 if (pilihan.contentEquals("n")){
				 break;
			 }
			 else  if (pilihan.contentEquals("y")){
				 continue;
			 }
			
		}while(true);
		
		
	}
	
	 void multiplication(){
		
		while(true){
			 int matrixMultiTotal=0,pilihan=0,baris1,kolom1,baris2,kolom2;
		 do{
			 do{
				 try{
					 System.out.print("The number of baris of matrix 1: "); //  baris1*kolom1   baris1   baris2*kolom2
					 baris1=input.nextInt();
					 System.out.print("The number of kolom of matrix 1: ");
					 kolom1=input.nextInt();
					 System.out.print("The number of baris of matrix 2: ");
					 baris2=input.nextInt();
					 System.out.print("The number of kolom of matrix 2: ");
					 kolom2=input.nextInt();
					 break;
				 }catch(InputMismatchException e){
					 System.out.println("Please input an integer");
					 input.nextLine();
					 continue;
				 }
			 }while(true);
			 
			
			 
					if (kolom1 != baris2) {
						System.out.println("The number of baris of matrix 1 does not equal the number of kolom of matrix 2");
						System.out.println("Matrices cannot be multiplied");					
						do{
							try{
								System.out.print("Press '0' to repeat the process or '1' to terminate : ");
								pilihan=input.nextInt();
								if (pilihan==1)break;
								if (pilihan==0)break;
							}catch(InputMismatchException e){
								System.out.println("Input one of the given number");
								input.nextLine();
								continue;
							}
							
						}while((pilihan!=0)||(pilihan!=1));
					}			
					if (pilihan==1)break;
					if (pilihan==0)continue;
		 }while(kolom1!=baris2);
			 if (pilihan==1)break;
			 System.out.println("\n");
			 
			 
		int[][]matrix=new int[baris1][kolom1];
		
		do{
			System.out.println("Matrix 1: ");
			for(int isibaris=0;isibaris<baris1;isibaris++){
				for(int isikolom=0;isikolom<kolom1;isikolom++){
					System.out.print("Row "+(isibaris+1)+" column "+(isikolom+1)+" : ");
					try{
						matrix[isibaris][isikolom]=input.nextInt();
					}catch(InputMismatchException e){
						System.out.println("Please input only an integer");
						input.nextLine();
						isikolom--;
						continue;
					}
					
				}
			}
			break;
		}while(true);
		
		
		System.out.println();
		
		for(int isibaris=0;isibaris<baris1;isibaris++){
			for(int isikolom=0;isikolom<kolom1;isikolom++){
				System.out.printf("%4d ", matrix[isibaris][isikolom]);
			}System.out.println();
		}System.out.println();
		
		
		
		
		
		
		
		int[][]matriks2=new int[baris2][kolom2];
		do{
			System.out.println("Matrix 2: ");
			for(int isibaris=0;isibaris<baris2;isibaris++){
				for(int isikolom=0;isikolom<kolom2;isikolom++){
					System.out.print("row "+(isibaris+1)+" column "+(isikolom+1)+" : ");
					try{
						matriks2[isibaris][isikolom]=input.nextInt();
					}catch(InputMismatchException e){
						System.out.println("Please input only an integer");
						input.nextLine();
						isikolom--;
						continue;
					}
					
				}
			}
			break;
		}while(true);
		
		
		
		System.out.println();
		
		
		
		for(int isibaris=0;isibaris<baris2;isibaris++){
			for(int isikolom=0;isikolom<kolom2;isikolom++){
				System.out.printf("%4d ", matriks2[isibaris][isikolom]);
			}System.out.println();
		}
		
		
		
		
		
		int[][]matriks3=new int[baris1][kolom2];
		for(int isibaris=0;isibaris<baris1;isibaris++){
			for(int isikolom=0;isikolom<kolom2;isikolom++){
				matrixMultiTotal=0;
				for(int multiplicationindex=0;multiplicationindex<kolom1;multiplicationindex++){
					matrixMultiTotal+=matrix[isibaris][multiplicationindex]*matriks2[multiplicationindex][isikolom];
				}
				matriks3[isibaris][isikolom]=matrixMultiTotal;
				
			}
				}System.out.println();
				
				
			
		System.out.println("The multiplication of matrices "+baris1+"x"+kolom1+" X "+baris2+"x"+kolom2+" will make up "+baris1+"x"+kolom2+" matrix");
		System.out.println("The result of the multiplication of both matrices is :\n");
		for(int isibaris=0;isibaris<baris1;isibaris++){
			for(int isikolom=0;isikolom<kolom2;isikolom++){
				this.multiply=matriks3[isibaris][isikolom];
				System.out.printf("%.2f ", multiply());
			}
			System.out.println();
		}
		
		System.out.println("\n");
		
		
		if(baris1 != kolom2){
			System.out.println("The result of the matrix doesn't have a determinant");
		}	
		else{
			if(baris1==2 && kolom2==2){
				if(((matriks3[0][0]*matriks3[1][1])-(matriks3[0][1]*matriks3[1][0]))!=0){
					System.out.println("The determinant of the matrix is "+((matriks3[0][0]*matriks3[1][1])-(matriks3[0][1]*matriks3[1][0])));
				}
				else{
					System.out.println("It's a mirrored matrix");
				}
			}
			else if(baris1==3 && kolom2==3){
				if(((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
						-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
						+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1]))))!=0){
					
					System.out.println("The determinant of the matrix is "+((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
							-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
							+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1])))));
				}
				else{
					System.out.println("It's a mirrored matrix");
				}
			}
		}
			
			
			do{
				try{
					System.out.print("Press '1' to repeat the process or '0' to terminate");
					pilihan=input.nextInt();
					if (pilihan==0)break;
					if (pilihan==1)break;
				}catch(InputMismatchException e){
					System.out.println("Input one of the given number");
					input.nextLine();
					continue;
				}
				
			}while((pilihan<0)||(pilihan>1));
			
			if (pilihan==0)break;
			if(pilihan==1)continue;
			System.out.println();
		 }
	}
	
	public static void main(String[] args){
		matriks panggil = new matriks();
		try{
			panggil.MainMenu();
		}catch(NoSuchElementException e){
			System.out.println("\n");
			System.out.println("You pressed Ctrl + Z");
			System.out.println("Program terminated");
		}		
		//nothing else to see here
    }	
}
class theMatrix{
	double matriks1,matriks2,multiply;
		double add(){
			return matriks1+matriks2;
	}
		double substract(){
			return matriks1-matriks2;
		}
		double multiply(){
			return multiply;
		}
	

}