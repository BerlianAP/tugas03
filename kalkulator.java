import java.util.Scanner;
public class kalkulator {
	 //membuat 3 buah variabel
    private int number1;
    private int number2;
    private int samadengan;
  
    //membuat method set dan get
    public void setnumber1(int n){ number1 = n; }
    public int getnumber1(){ return number1; }
  
    public void setnumber2(int n){ number2 = n; }
    public int getnumber2(){ return number2; }
  
    //karena kita tidak boleh mengeset hasil dari luar,
    //maka kita hanya membuat getHasil
    public int getsamadengan(){ return samadengan; }
  
    //membuat langkah proses
    public void hitungPenjumlahan(){
        samadengan = number1+number2;
    }
  
    public void hitungperkalian(){
        samadengan = number1*number2;
    }
    public void hitungpembagian(){
        samadengan = number1/number2;
    }
    public void hitungPengurangan(){
        samadengan = number1-number2;
    }


    public static void main(String[] args){
    
        //membuat objek baru
        kalkulator x = new kalkulator();
        Scanner input = new Scanner(System.in);
        boolean lanjut = true;
      
        //penerapan konsep perulangan while supaya menunya berulang
        while(lanjut==true){
            System.out.println("");
            System.out.println("=========================");
            System.out.println("Kalkulator");
            System.out.println("1. Penjumlahan");
            System.out.println("2. Perkalian");
            System.out.println("3. Pembagian");
            System.out.println("4. Pengurangan");
            System.out.println("`````````````````````````");
            System.out.print("masukkan pilihan kamu = ");
            int pilihan = input.nextInt();
            System.out.println("");
          
            System.out.print("Masukkan angka pertama = ");
            int number1 = input.nextInt();
            x.setnumber1(number1);
            System.out.println("");
          
            System.out.print("Masukkan bilangan kedua = ");
            int number2 = input.nextInt();
            x.setnumber2(number2);
            System.out.println("");
          
            //penerapan konsep percabangan if
            if(pilihan==1){
                x.hitungPenjumlahan();
                System.out.println("Hasil Penjumlahan = "+x.getsamadengan());
            }
            else if(pilihan==2){
                x.hitungperkalian();
                System.out.println("Hasil Pengurangan = "+x.getsamadengan());
            }
            else if(pilihan==3){
                x.hitungpembagian();
                System.out.println("Hasil Pengurangan = "+x.getsamadengan());
            }
            else if(pilihan==4){
                x.hitungPengurangan();
                System.out.println("Hasil Pengurangan = "+x.getsamadengan());
            }
            else if(pilihan<1 || pilihan>3)// tanda || artinya adalah "atau"
                System.out.println("Input anda salah");
            else
                lanjut=false;
        }
    }

}